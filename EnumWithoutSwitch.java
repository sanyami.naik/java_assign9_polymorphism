package Assignments.nine;
import java.util.Enumeration;


enum SignalColourr {

    RED("RED MEANS STOP"),
    GREEN("GREEN MEANS GO"),
    YELLOW("YELLOW MEANS DRIVE VERY SLOW");

    public final String label;
    SignalColourr(String s)
    {
        this.label=s;
    }

}
    public class EnumWithoutSwitch {

    public static void main(String[] args) {



        for(SignalColourr e:SignalColourr.values()){
            System.out.println(e+" = "+e.label);
        }

    }
}



/*
OUTPUT:
RED = RED MEANS STOP
GREEN = GREEN MEANS GO
YELLOW = YELLOW MEANS DRIVE VERY SLOW
 */