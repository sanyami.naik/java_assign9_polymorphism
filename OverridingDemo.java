package Assignments.nine;

class Parent{
    Object display()
    {
        System.out.println("In the parent display method");
        return this;
    }

}

class Child extends Parent{
    String display()
    {
        System.out.println("In the child display method");
        return ("Sanyami");
    }
}

public class OverridingDemo {
    public static void main(String[] args) {
        {
            Child c=new Child();
            c.display();
        }
    }
}



/*
OUTPUT:
1)If the return types are completely different that is they are not covarient then we will definitly get error of
incompatible return types
java: display() in Assignments.nine.Child cannot override display() in Assignments.nine.Parent
  return type void is not compatible with int


2)If the return type of the overriden method is covarient that is of subclass type of the overridden method then
overriding occurs
In the child display method
 */