package Assignments.nine;
/*
class Test{
    void show(int a)
    {
        System.out.println("In int method");
    }
    int show(int a)
    {
        System.out.println("In int method");
    }
    int show(int,int b)
    {

    }
}


public class OverloadingDemo {
    public static void main(String[] args) {
        Test t=new Test();
        t.show(10);

    }
}



/*
OUTPUT:
java: method show(int) is already defined in class Assignments.nine.Test

when we change the return type overloading doesnt occur as the compiler only chcks for method signature to be different.
 */