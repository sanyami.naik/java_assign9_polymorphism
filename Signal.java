package Assignments.nine;

import java.util.Enumeration;


enum SignalColour {

    RED,
    GREEN,
    YELLOW


}

public class Signal {
    public static void main(String[] args) {

        SignalColour myVar = SignalColour.GREEN;

        switch (myVar) {
            case RED:
                System.out.println("RED MEANS STOP");
                break;
            case GREEN:
                System.out.println("GREEN MEANS GO");
                break;
            case YELLOW:
                System.out.println("YELLOW MEANS DRIVE VERY SLOW");
                break;

        }

        for(SignalColour e:SignalColour.values()){
            System.out.println(e);
        }

    }
}