package Assignments.nine;

class Addition{
     void add(int a,int b)
     {
         System.out.println("In int,int "+ (a+b));
     }
    void add(int a,int b,int c)
    {
        System.out.println("In int,int,int "+ (a+b));
    }
    void add(int a,float b)
    {
        System.out.println("In int,float "+ (a+b));
    }
    void add(float a,int b)
    {
        System.out.println("In float,int "+ (a+b));
    }
}

class VarargsAddition{

     void add(int ...varargs)
     {
         int sum=0;
         for(int i=0;i<varargs.length;i++)
         {
             sum=sum+varargs[i];
         }
         System.out.println("In the varargs "+sum);

     }

}
public class MainApplication {

    public static void main(String[] args)
        {
            Addition addition=new Addition();
            addition.add(10,20);
            addition.add(34,56,32);
            addition.add(43.5f,87);
            addition.add(67,45.56f);


            VarargsAddition varargsAddition=new VarargsAddition();
            varargsAddition.add(67,34,98,67,87,56,433);

        }


}
