package Assignments.nine;

public class Shape {

    void calculateArea(float radius)
    {
        double area=3.14*radius*radius;
        System.out.println("The area of circle is "+String.format("%.2f", area)+"sq.cm");
    }
    void calculateArea(int side)
    {
        System.out.println("The area of square is "+side*side+"sq.cm");
    }
    void calculateArea(int length,int breadth)
    {
        System.out.println("The area of rectangle is "+length*breadth+"sq.cm");
    }
    void calculateArea(int base,float height)
    {
        double area=0.5*base*height;
        System.out.println("The area of triangle is "+String.format("%.2f",area)+"sq.cm");
    }

    public static void main(String[] args) {

        Shape shape=new Shape();
        shape.calculateArea(45);
        shape.calculateArea(65.7f);
        shape.calculateArea(20,40);
        shape.calculateArea(34,23.8f);

    }
}


/*
OUTPUT:
The area of square is 2025sq.cm
The area of circle is 13553.78sq.cm
The area of rectangle is 800sq.cm
The area of triangle is 404.60sq.cm
 */